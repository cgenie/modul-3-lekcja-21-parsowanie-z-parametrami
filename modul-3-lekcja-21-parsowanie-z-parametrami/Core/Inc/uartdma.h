/*
 * uartdma.h
 *
 *  Created on: Nov 3, 2020
 *      Author: przemek
 */

#ifndef INC_UARTDMA_H_
#define INC_UARTDMA_H_

#define DMA_RX_BUFFER_SIZE 256
#define DMA_TX_BUFFER_SIZE 256

#include "ring_buffer.h"

typedef struct {
	UART_HandleTypeDef* huart;

	// Receive buffer
	uint8_t DMA_RX_Buffer[DMA_RX_BUFFER_SIZE];
	Ringbuffer UART_RX_Buffer;
	uint8_t uartRxBufferLines;

	// Transmit buffer
	uint8_t DMA_TX_Buffer[DMA_TX_BUFFER_SIZE];
	Ringbuffer UART_TX_Buffer;
	uint8_t uartTxBufferLines;
} UARTDMA_HandleTypeDef;

void UARTDMA_UartIrqHandler(UARTDMA_HandleTypeDef *huartdma);
void UARTDMA_DmaReceiveIrqHandler(UARTDMA_HandleTypeDef *huartdma);

uint8_t UARTDMA_PutCharToTxBuffer(UARTDMA_HandleTypeDef *huartdma, char c);
void UARTDMA_Print(UARTDMA_HandleTypeDef *huartdma, char *message);
uint8_t UARTDMA_GetLineFromRxBuffer(UARTDMA_HandleTypeDef *huartdma, char *outBuffer);
uint8_t UARTDMA_IsDataRxReady(UARTDMA_HandleTypeDef *huartdma);
uint8_t UARTDMA_IsDataTxReady(UARTDMA_HandleTypeDef *huartdma);
void UARTDMA_TransmitEvent(UARTDMA_HandleTypeDef *huartdma);

void UARTDMA_Init(UARTDMA_HandleTypeDef *huartdma, UART_HandleTypeDef *huart);

#endif /* INC_UARTDMA_H_ */
