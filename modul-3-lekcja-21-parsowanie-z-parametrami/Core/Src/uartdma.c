/*
 * uartdma.c
 *
 *  Created on: Nov 3, 2020
 *      Author: przemek
 */


#include "main.h"
#include "uartdma.h"

typedef struct {
	__IO uint32_t ISR;  // DMA interrupt status register
	__IO uint32_t Reserved0;
	__IO uint32_t IFCR;  // DMA interrupt flag clear register
} DMA_Base_Registers;

void UARTDMA_UartIrqHandler(UARTDMA_HandleTypeDef *huartdma) {
	if(huartdma->huart->Instance->SR & UART_FLAG_IDLE) {  // check if idle flag is set (see Reference Manual, USART section, status registers)
		volatile uint32_t tmp;
		tmp = huartdma->huart->Instance->SR;  // Read status register
		tmp = huartdma->huart->Instance->DR;  // Read data register

		// In STM32F4 we have DMA Streams, not Channels
		// Stopping a DMA Stream triggers the DMA Transfer Complete IRQ
		huartdma->huart->hdmarx->Instance->CR &= ~DMA_SxCR_EN;  // Disable DMA - it will force Transfer Complete

		tmp = tmp;  // For unused warning in the compiler
	}
}

void UARTDMA_DmaReceiveIrqHandler(UARTDMA_HandleTypeDef *huartdma) {
	uint8_t *dmaBufferPointer;
	uint16_t i;
	uint16_t length;

	DMA_Base_Registers *dmaRegisters = (DMA_Base_Registers *) huartdma->huart->hdmarx->StreamBaseAddress;  // take registers base address

	if(__HAL_DMA_GET_IT_SOURCE(huartdma->huart->hdmarx, DMA_IT_TC) != RESET) {  // Check if interrupt source
		// IFCR flag in DMA registers
		dmaRegisters->IFCR = DMA_FLAG_TCIF0_4 << huartdma->huart->hdmarx->StreamIndex;  // clear transfer code

		length = DMA_RX_BUFFER_SIZE - huartdma->huart->hdmarx->Instance->NDTR;  // get the length of transfer

		dmaBufferPointer = huartdma->DMA_RX_Buffer;

		for(i = 0; i < length; i++) {
			rbWrite(&huartdma->UART_RX_Buffer, dmaBufferPointer[i]);

			if(dmaBufferPointer[i] == '\n') {
				huartdma->uartRxBufferLines++;
			}
		}

		dmaRegisters->IFCR = 0x3FU << huartdma->huart->hdmarx->StreamIndex;  // clear all interrupts
		huartdma->huart->hdmarx->Instance->M0AR = (uint32_t) huartdma->DMA_RX_Buffer;  // set memory address
		huartdma->huart->hdmarx->Instance->NDTR = DMA_RX_BUFFER_SIZE;
		huartdma->huart->hdmarx->Instance->CR |= DMA_SxCR_EN;  // start our transfer
	}
}

// put one char to UART transmit buffer
uint8_t UARTDMA_PutCharToTxBuffer(UARTDMA_HandleTypeDef *huartdma, char c) {
	if(RB_OK != rbWrite(&huartdma->UART_TX_Buffer, c)) {
		return 1;
	}

	return 0;
}

// put message to UART buffer
void UARTDMA_Print(UARTDMA_HandleTypeDef *huartdma, char *message) {
	char *msgPointer;
	char charToPut;

	msgPointer = (char*) message;

	while ((charToPut = *msgPointer)) {
		UARTDMA_PutCharToTxBuffer(huartdma, charToPut);
		msgPointer++;

		if(charToPut == '\n') {
			huartdma->uartTxBufferLines++;
		}
	}
}

uint8_t UARTDMA_GetLineFromRxBuffer(UARTDMA_HandleTypeDef *huartdma, char *outBuffer) {
	char *outBufferPtr = outBuffer;

	if(huartdma->uartRxBufferLines) {
		while ((RB_OK == rbRead(&huartdma->UART_RX_Buffer, (uint8_t *)outBufferPtr))) {
			if(*outBufferPtr == '\n') {
				*outBufferPtr = 0;  // end of string in C
				huartdma->uartRxBufferLines--;
				break;
			}
			outBufferPtr++;
		}
		return 0;
	}

	return 1;
}

uint8_t UARTDMA_IsDataRxReady(UARTDMA_HandleTypeDef *huartdma) {
	if(huartdma->uartRxBufferLines) {
		return 1;
	}

	return 0;
}

uint8_t UARTDMA_IsDataTxReady(UARTDMA_HandleTypeDef *huartdma) {
	if(huartdma->uartTxBufferLines) {
		return 1;
	}

	return 0;
}

void UARTDMA_TransmitEvent(UARTDMA_HandleTypeDef *huartdma) {
	char charToSend;
	uint16_t i = 0;

	if(UARTDMA_IsDataTxReady(huartdma)) {
		if(huartdma->huart->hdmatx->State != HAL_DMA_STATE_BUSY) {  // is DMA ready to transmit?
			while(RB_OK == rbRead(&huartdma->UART_TX_Buffer, (uint8_t *) &charToSend)) {
				if(charToSend == '\n') {
					huartdma->uartTxBufferLines--;
				}
				huartdma->DMA_TX_Buffer[i] = charToSend;
				i++;
			}

			HAL_UART_Transmit_DMA(huartdma->huart, huartdma->DMA_TX_Buffer, i);
		}
	}
}

// Initialization
void UARTDMA_Init(UARTDMA_HandleTypeDef *huartdma, UART_HandleTypeDef *huart) {
	huartdma->huart = huart;

	// IDLE Enable
	__HAL_UART_ENABLE_IT(huartdma->huart, UART_IT_IDLE);

	// DMA Transfer Complete Enable
	__HAL_DMA_ENABLE_IT(huartdma->huart->hdmarx, DMA_IT_TC);
	__HAL_DMA_ENABLE_IT(huartdma->huart->hdmatx, DMA_IT_TC);

	// Run DMA for whole buffer
	HAL_UART_Receive_DMA(huartdma->huart, huartdma->DMA_RX_Buffer, DMA_RX_BUFFER_SIZE);

	// DMA Half Transfer Disable
	__HAL_DMA_DISABLE_IT(huartdma->huart->hdmarx, DMA_IT_HT);
	__HAL_DMA_DISABLE_IT(huartdma->huart->hdmatx, DMA_IT_HT);

	// Run DMA UART on Buffer RX
}
