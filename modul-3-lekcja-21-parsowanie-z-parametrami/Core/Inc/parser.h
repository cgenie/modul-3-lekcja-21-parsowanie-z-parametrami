/*
 * parser.h
 *
 *  Created on: Nov 10, 2020
 *      Author: przemek
 */

#ifndef INC_PARSER_H_
#define INC_PARSER_H_

#include "uartdma.h"

void UART_ParseLine(UARTDMA_HandleTypeDef *huartdma);


#endif /* INC_PARSER_H_ */
