/*
 * ring_buffer.h
 *
 *  Created on: Oct 30, 2020
 *      Author: przemek
 */

#ifndef INC_RING_BUFFER_H_
#define INC_RING_BUFFER_H_

#define RING_BUFFER_SIZE 128

typedef enum {
	RB_OK = 0,
	RB_ERROR = 1
} RbStatus;

typedef struct {
	uint8_t buffer[RING_BUFFER_SIZE];
	uint8_t head;
	uint8_t tail;
} Ringbuffer;

RbStatus rbRead(Ringbuffer *rb, uint8_t *value);
RbStatus rbWrite(Ringbuffer *rb, uint8_t value);

#endif /* INC_RING_BUFFER_H_ */
