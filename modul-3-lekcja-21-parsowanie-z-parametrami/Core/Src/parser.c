/*
 * parser.c
 *
 *  Created on: Nov 10, 2020
 *      Author: przemek
 */

#include "main.h"

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "parser.h"
#include "uartdma.h"

char myName[32];

uint8_t isDigit(char v) {
	if(v >= '0' && v <= '9') {
		return 1;
	}

	return 0;
}

// ENV=X,Y,Z\0 // X - temperature, Y - humidity, Z - pressure
void UART_ParseENV(UARTDMA_HandleTypeDef *huartdma) {
	int i, j;
	char message[64];
	float envParameters[3];

	for(i = 0; i < 3; i++) {
		char* value = strtok(NULL, ",");  // strtok magic - it remembers where it was last time

		if(strlen(value) > 0) {
			for(j = 0; value[j] != 0; j++) {
				if((isDigit(value[j]) == 0) && (value[j] != '.')) {
					sprintf(message, "ENV wrong value. Don't use letters\n");
					UARTDMA_Print(huartdma, message);
					return;
				}
			}

			envParameters[i] = atof(value);
		} else {
			sprintf(message, "ENV not enough values. ENV=X,Y,Z\n");
			UARTDMA_Print(huartdma, message);
		}
	}

	sprintf(message, "ENV Temperature: %.1f\n", envParameters[0]);
	UARTDMA_Print(huartdma, message);
	sprintf(message, "ENV Humidity: %.1f\n", envParameters[1]);
	UARTDMA_Print(huartdma, message);
	sprintf(message, "ENV Pressure: %.1f\n", envParameters[2]);
	UARTDMA_Print(huartdma, message);
}

// LED=0\0; LED=1\0
void UART_ParseLED(UARTDMA_HandleTypeDef *huartdma) {
	char* value = strtok(NULL, "=");  // strtok magic - it remembers where it was last time

	if(strlen(value) > 0) {
		if(strcmp(value, "1") == 0) {
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
			UARTDMA_Print(huartdma, "LED ON\n");
		} else if(strcmp(value, "0") == 0) {
			HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
			UARTDMA_Print(huartdma, "LED OFF\n");
		}
	}
}

// NAME=X\0
// NAME=?\0 to ask for current name
void UART_ParseName(UARTDMA_HandleTypeDef *huartdma) {
	char* value = strtok(NULL, ",");  // strtok magic - it remembers where it was last time
	char message[64];

	if(strlen(value) > 0) {
		if(strcmp(value, "?") == 0) {
			sprintf(message, "Name: %s\n", myName);
		} else {
			strcpy(myName, value);
			sprintf(message, "Name changed to %s\n", myName);
		}
	} else {
		sprintf(message, "Name cannot be empty\n");
	}

	UARTDMA_Print(huartdma, message);
}

void UART_ParseLine(UARTDMA_HandleTypeDef *huartdma) {
	char bufferReceive[64];

	if(!UARTDMA_GetLineFromRxBuffer(huartdma, bufferReceive)) {
		char* parsePointer = strtok(bufferReceive, "=");  // LED=1\0
		// parsePointer = LED\0

		if(strcmp(parsePointer, "LED") == 0) {
			UART_ParseLED(huartdma);
		} else if(strcmp(parsePointer, "ENV") == 0) {
			UART_ParseENV(huartdma);
		} else if(strcmp(parsePointer, "NAME") == 0) {
			UART_ParseName(huartdma);
		} else {
			UARTDMA_Print(huartdma, "Unknown token\n");
		}
	}
}
